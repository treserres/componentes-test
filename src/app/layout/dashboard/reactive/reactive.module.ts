import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveComponent } from './reactive.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ReactiveComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: ':componente', component: ReactiveComponent }
    ])
  ]
})
export class ReactiveModule { }
