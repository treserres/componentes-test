import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'tr-reactive',
	templateUrl: './reactive.component.html',
	styleUrls: ['./reactive.component.scss']
})
export class ReactiveComponent implements OnInit {
	componente: string;

	constructor(
		private activatedRoute: ActivatedRoute
	) {
		this.activatedRoute.params.subscribe(params => {
			this.componente = params.componente;
			console.log(this.componente);
		});
	}

	ngOnInit() {
		
	}

	mostrar(componente: string) {
		return this.componente === componente || this.componente === 'form';
	}

}
