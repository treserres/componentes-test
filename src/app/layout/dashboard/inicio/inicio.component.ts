import { Component, OnInit } from '@angular/core';
import { HttpService } from '@app/core';

interface ICommit {
	author_email: string;
	author_name: string;
	authored_date: string;
	committed_date: string;
	committer_email: string;
	committer_name: string;
	created_at: string;
	id: string;
	message: string;
	parent_ids: string[];
	short_id: string;
	title: string;
}

@Component({
	selector: 'tr-inicio',
	templateUrl: './inicio.component.html',
	styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

	constructor(private httpService: HttpService) { }

	commits: ICommit[] = [];
	loading = false;

	ngOnInit() {
		this.loading = true;
		this.httpService.get('https://gitlab.com/api/v4/projects/11554864/repository/commits').then((commits: ICommit[]) => {
			this.commits = commits;
			this.loading = false;
		});
	}

	open(commitId: string) {
		window.open('https://gitlab.com/treserres/componentes/commit/' + commitId);
	}

}
