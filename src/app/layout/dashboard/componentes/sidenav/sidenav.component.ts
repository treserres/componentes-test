import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { collapsible } from '@app/shared';
import { DashboardService } from '../../dashboard.service';
import { MatSidenav } from '@angular/material';
import { Page, Section } from './interfaces';
import { Router, NavigationEnd } from '@angular/router';
import { UtilService } from '@app/core';

@Component({
	selector: 'tr-sidenav',
	templateUrl: './sidenav.component.html',
	styleUrls: ['./sidenav.component.scss'],
	animations: [collapsible]
})
export class SidenavComponent implements OnInit {

	constructor(
		private dashboardService: DashboardService,
		private utilService: UtilService,
		private router: Router
	) { }

	user = this.utilService.getLS('user', true);

	sections: Section[] = [
		{
			title: '',
			children: [
				{ url: '/inicio', title: 'Inicio' },
			]
		},
		{
			title: 'formulario',
			children: [
				{ url: '/template/form', title: 'Template driven', icon: 'reorder' },
				{ url: '/reactive/form', title: 'Reactive forms', icon: 'reorder' },
			]
		},
		{
			title: 'componentes',
			children: [
				{
					title: 'autocomplete',
					icon: 'edit',
					pages: [
						{ url: '/template/autocomplete', title: 'Template driven' },
						{ url: '/reactive/autocomplete', title: 'Reactive forms' }
					]
				},
				{
					title: 'fecha',
					icon: 'today',
					pages: [
						{ url: '/template/fecha', title: 'Template driven' },
						{ url: '/reactive/fecha', title: 'Reactive forms' }
					]
				},
				{
					title: 'selectormes',
					icon: 'done',
					pages: [
						{ url: '/template/selectormes', title: 'Template driven' },
						{ url: '/reactive/selectormes', title: 'Reactive forms' }
					]
				}
			]
		}
	];

	@ViewChild(MatSidenav) snav: MatSidenav;

	ngOnInit() {
		if (document.documentElement.clientWidth <= 768) {
			setTimeout(() => {
				this.snav.close();
			});
		}
		this.dashboardService.onSidenavToggle.subscribe(() => {
			this.snav.toggle();
		});
		this.sections.forEach((section: Section) => {
			section.children.forEach((child: any) => {
				if (child.pages) {
					child.pages.forEach((page: Page) => {
						if (window.location.pathname.includes(page.url)) {
							child.expanded = true;
						}
					});
				}
			});
		});
		this.dashboardService.isSnavOpened = this.snav.opened;
		this.router.events.subscribe((event) => {
			if (event instanceof NavigationEnd) {
				if (window.innerWidth <= 768 && this.snav.opened) {
					this.snav.close();
				}
			}
		})
	}

	isActive(page: Page) {
		return this.router.url === page.url;
	}

	@HostListener('window:resize', ['$event'])
	onResize(event) {
		if (event.target.innerWidth <= 768 && this.snav.opened) {
			this.snav.close();
		}
	}

}
