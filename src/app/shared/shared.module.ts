import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UtilsModule } from '@tres-erres/ngx-utils';

/* Material Modules */
import {
	MatButtonModule,
	MatInputModule,
	MatCardModule,
	MatProgressSpinnerModule,
	MatTooltipModule,
	MatSnackBarModule,
	MatDialogModule,
	MatToolbarModule,
	MatIconModule,
	MatCheckboxModule,
	MatSelectModule,
	MatTableModule,
	MatFormFieldModule,
	MatRadioModule,
	MatSidenavModule,
	MatListModule,
	MatMenuModule
} from '@angular/material';

/** Modules */


/** Directives */
import { FocusNextDirective } from './focus-next.directive';

import { HttpService } from '@app/core';

import { ComponentesModule, HttpService as ComponentesHttpService } from '@componentes/.';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,

		/* Custom Modules */
		ComponentesModule,

		/* Material Modules */
		MatFormFieldModule,
		MatButtonModule,
		MatInputModule,
		MatCardModule,
		MatProgressSpinnerModule,
		MatTooltipModule,
		MatSnackBarModule,
		MatDialogModule,
		MatToolbarModule,
		MatIconModule,
		MatCheckboxModule,
		MatSelectModule,
		MatTableModule,
		MatRadioModule,
		MatSidenavModule,
		MatListModule,
		MatMenuModule,

		UtilsModule
	],
	declarations: [
		/* Directives */
		FocusNextDirective,

	],
	exports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,

		/* Custom Modules */
		ComponentesModule,
		UtilsModule,

		/* Material Modules */
		MatFormFieldModule,
		MatButtonModule,
		MatInputModule,
		MatCardModule,
		MatProgressSpinnerModule,
		MatTooltipModule,
		MatSnackBarModule,
		MatDialogModule,
		MatToolbarModule,
		MatIconModule,
		MatCheckboxModule,
		MatSelectModule,
		MatTableModule,
		MatRadioModule,
		MatSidenavModule,
		MatListModule,
		MatMenuModule,

		/** Directives */
		FocusNextDirective,


	],
	entryComponents: [

	],
	providers: [
		{ provide: ComponentesHttpService, useClass: HttpService }
	]
})
export class SharedModule { }
